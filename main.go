package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func main() {
	// Collect flags
	injectVar := flag.String("var", "INJECTED_VAR", "Name of injectable variable to replace")
	k8sFile := flag.String("f", "k8s-deployment.yml", "Relative path to your Kubernetes YAML file")
	flag.Parse()

	// Get the value from the build env vars that GitLab CI sets
	fetchedValue := evaluateVariable(*injectVar)

	// Edit the Kubernetes file
	editK8sFile(*k8sFile, *injectVar, fetchedValue)
}

func evaluateVariable(injectVar string) string {
	environmentName := os.Getenv("CI_ENVIRONMENT_NAME")
	environmentNameUpper := strings.ToUpper(environmentName)
	environmentNameUpperSanitized := strings.Replace(environmentNameUpper, "-", "_", -1)
	baseVarName := strings.Replace(injectVar, "INJECTED_", "", -1)

	// Concatenate the stripped variable with the uppercased environment
	concatVar := join(baseVarName, "_", environmentNameUpperSanitized)

	// Use the built string as a key for fetching a GitLab CI build var
	value := os.Getenv(concatVar)

	return value
}

func editK8sFile(k8sFile string, injectVar string, fetchedValue string) {
	read, err := ioutil.ReadFile(k8sFile)
	if err != nil {
		log.Fatal(err)
	}

	newContents := strings.Replace(string(read), injectVar, fetchedValue, -1)

	err = ioutil.WriteFile(k8sFile, []byte(newContents), 0)
	if err != nil {
		log.Fatal(err)
	}
}

func join(strs ...string) string {
	var sb strings.Builder
	for _, str := range strs {
		sb.WriteString(str)
	}
	return sb.String()
}
