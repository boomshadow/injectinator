[![forthebadge](https://forthebadge.com/images/badges/made-with-go.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/powered-by-jeffs-keyboard.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/check-it-out.svg)](https://forthebadge.com)

# About
<https://gitlab.com/boomshadow/injectinator>  
Simple environment variable injection, written in Golang. **Note**: It is designed for those using `GitLab CI` and `Kubernetes`.

## Usage
```
injectinator -var INJECTED_PASSWORD -f my-kubernetes.yml
```

`-var` specifies the variable you wish to replace. It _must_ be prefixed with `INJECTED_`. You **must** have an accompanying uppercase variable with environment in your GitLab UI. Example: if you wish to substitute `INJECTED_PASSWORD` you must have `PASSWORD_DEVELOPMENT` specified in GitLab UI.

The environment name is discovered automatically, so you **must** set the environment name in your `.gitlab-ci.yml` file (See example code blocks below)

The -f flag specifies your filename. The path to the file is relative to where you are running the command from.

## TL;DR
`injectinator` allows you to follow the convention of all upper case environment names, configs set in GitLab CI, and use separate credentials for separate environments. The goal is to [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) up your config injection code as much as possible, in order to eliminate human errors.

## More detailed 'About'
`injectinator` is meant to assist those using GitLab CI and Kubernetes. In GitLab CI/ you can set all your application configs and secrets in the GitLab interface (System >> CI/CD >> Variables). Then, you can access those variables easily in the `deploy` stage of your pipeline simply by accessing the variable name in shell, like you would with any variable (ex: `$VARIABLE`).

Assuming you're a responsible developer, you would have different environments for your deployments (Ex: development, test, QA, production), each needing different credentials, because credential sharing accross environments is bad, duh. 

You're further still a good developer in that you believe in building immutable Docker images, that can be shipped to any environment. Clean, reproducible builds, with no surprise whackiness done per environment during the build phase. Each image need only have its configs changed in order to work in a different environment.

At my company, the approach was to have placeholder 'INJECT_' values in the Kubernetes YML file, and replace them with `sed` during the deployment phase. This would result in many, complicated `sed` commands with hacky string parsing that had to be done beforehand sometimes. It was ugly, and when you added a new variable to the Kubernetes yaml, you can to copy/paste/edit the `sed` command to each environment. This was very manual, and quite error prone.

### The old way of doing things
In your GitLab interface, you set your database passwords like so:

```
DATBASE_PASSWORD_DEVELOPMENT
DATBASE_PASSWORD_TEST
DATBASE_PASSWORD_QA
DATBASE_PASSWORD_PRODUCTION
```

In your `.gitlab-ci.yml` file, you have the injection like so:

```
deploy:development:
  stage: deploy
  script:
    - sed -i "s|INJECTED_DATBASE_PASSWORD|$DATBASE_PASSWORD_DEVELOPMENT|g" k8s-deployment.yml
    - kubectl apply -f k8s-deployment.yml
  environment:
    name: development

deploy:production:
  stage: deploy
  script:
    - sed -i "s|INJECTED_DATBASE_PASSWORD|$DATBASE_PASSWORD_PRODUCTION|g" k8s-deployment.yml
    - kubectl apply -f k8s-deployment.yml
  environment:
    name: production
```

This is unecessarily complex. We already know the environment name from the `.gitlab-ci.yml` manifest. We should be able to use that to get the variable and inject that into the `k8s-deployment.yml`

### New hotness
`injectinator` allows you to follow the convention of all upper case environment names and separate credentials for separate environments, while eliminating the manual changes from job to job. See the result below:

```
deploy:development:
  stage: deploy
  script:
    - injectinator -var INJECTED_DATBASE_PASSWORD
    - kubectl apply -f k8s-deployment.yml
  environment:
    name: development

deploy:production:
  stage: deploy
  script:
    - injectinator -var INJECTED_DATBASE_PASSWORD
    - kubectl apply -f k8s-deployment.yml
  environment:
    name: production
```

Notice how the injection command is _exactly_ the same for both development and production? `injectinator` picks up on the GitLab job environment and uses that to parse the desired value. You can now copy/paste the injection command to as many environments as you need, without introducing human error of typing the env name wrong.

# Downloads
Pre-made binaries are created with each release. You can find the files as part of the Git tags here:

<https://gitlab.com/boomshadow/injectinator/tags>

# Building

## Dependencies
- golang (duh)
- goreleaser (only needed for building _all_ the binaries -- https://goreleaser.com/install/)

## Compiling

    go build
    # For building all the binaries:
    goreleaser release --skip-publish

# Reading/Reference
- https://gobyexample.com/command-line-flags
- https://golang.org/pkg/flag/
- https://www.calhoun.io/concatenating-and-building-strings-in-go/
- https://gist.github.com/tdegrunt/045f6b3377f3f7ffa408

# Copyright
This is free and unencumbered software released into the public domain.  
See LICENSE.md or [unlicense.org](http://unlicense.org) for more information.
